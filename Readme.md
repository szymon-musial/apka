# Moja jedna z pierwszych praktyczna aplikacji w `C#`

## Motywacja

Od gimnazjum eksperymentowałem z językami programowania.

Zwiększałem swoje kompetencje w językach takich jak:
- c++
- Js
- php

Po czym zdobywając kompetencję technika elektronika zainteresowałem się Arduino gdzie rozwijałem `c++` oraz opcją w `VS 2015` WinForms.

Od tego kliknięcia rozpocząłem praktycznie wgłębianie się w `C#` co w klasie maturalnej skutkowało napisaniem aplikacji znajdującej się w tym repozytorium używając do tego [Xamarin, a dokładniej Xamarin.Android](https://learn.microsoft.com/en-us/xamarin/android/)

Projekt był rozwijany do tego momentu.

## Funkcjonalność aplikacji

Jej zadaniem jest obliczanie kosztów trasy zdefiniowanym przez użytkownika pojazdem.

Do wykonania kalkulacji oczekuje się wpisanych cen paliw.

Aby dokonać obliczeń użytkownik albo wpisuje tymczasowe spalanie pojazdu, albo korzysta z wcześniej zdefiniowanych maszyn.

Istnieją dwie możliwości obliczeń:
- Obliczenie kosztów przejazdu na podstawie wprowadzonej długości trasy,
- Obliczenie możliwej do przejechania trasy na podstawie założonych kosztów trasy.

Ponadto w aplikacji 'Google Maps' można udostępnić trasę do opisywanej aplikacji. Wtedy `Intent` w aplikacji odczyta długość trasy i uzupełni pole `Droga` eliminując konieczność ręcznego przepisywania.

#### Ponadto pragnę zaznaczyć że tą aplikację pisałem w dawno i od tego moje kompetencje uległy zmianie

# Zrzuty ekranu

|                                                                                |                                                                                           |
| ------------------------------------------------------------------------------ | ----------------------------------------------------------------------------------------- |
| ![Główna funkcjonalność](./Screens/Screenshot_20200121-191558_Road_Cost.jpg)   | ![Kalkulacje z doskoku](./Screens/Screenshot_20200121-191345_Road_Cost.jpg)               |
| ![Floating Action Button](./Screens/Screenshot_20200121-191428_Road_Cost.jpg)  | ![Dodawanie pojazdu](./Screens/Screenshot_20200121-191420_Road_Cost.jpg)                  |
| ![Usuwanie pojazdu](./Screens/Screenshot_20200121-191438_Road_Cost.jpg)        | ![Udostępnianie lokalizacji z Google Maps](./Screens/Screenshot_20200121-191543_Maps.jpg) |
